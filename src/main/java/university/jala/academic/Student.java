package university.jala.academic;

import java.util.Comparator;

/**
 * Student.
 */
public class Student {
    private String name;
    private int grade;

    public Student(String name, int grade) {
        this.name = name;
        this.grade = grade;
    }

    public String getName() {
        return name;
    }

    public int getGrade() {
        return grade;
    }

    @Override
    public String toString() {
        return "{" + name + "," + grade + "}";
    }

    // Método estático para comparar por grado
    public static Comparator<Student> byGrade() {
        return Comparator.comparingInt(Student::getGrade);
    }

}
