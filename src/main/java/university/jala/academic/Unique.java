package university.jala.academic;
/**
 * Unique.
 */
public interface Unique<T> {
    void unique();
}