package university.jala.academic;

import java.util.*;

import java.util.Comparator;
import java.util.Iterator;


//<T> -> Indicates generic class
public class UniArrayList<T> implements Sortable<T>, Unique<T>, List<T> {

    private T[] ints;


    public UniArrayList(T[] array) {
        this.ints = (T[]) new Object[array.length];
        for (int i = 0; i < array.length; i++) {
            this.ints[i] = array[i];
        }
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int i = 0; i < ints.length; i++) {
            sb.append(ints[i]);
            if (i < ints.length - 1) {
                sb.append(", ");
            }
        }
        sb.append("]");
        return sb.toString();
    }


    private int size;

    //Sortable
    @Override
    public void sort() {
        //logic
    }

    @Override
    public void sortBy(Comparator<T> comparator) {
        //logic
    }

    //Unique
    @Override
    public void unique() {
        //logic
    }

    //Methods
    @Override
    public int size() {
        return ints.length;
    }

    @Override
    public boolean isEmpty() {
        return ints.length == 0;
    }

    @Override
    public boolean contains(Object o) {
        for (T element : ints) {
            if (element.equals(o)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        // Implementa este método
        return null;
    }

    @Override
    public Object[] toArray() {
        // Implementa este método
        return null;
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        // Implementa este método
        return null;
    }

    @Override
    public boolean add(T t) {
        return false;
    }

    @Override
    public void add(int index, T element) {
        if (index < 0 || index > ints.length) {
            throw new IndexOutOfBoundsException();
        }
        T[] newArray = (T[]) new Object[ints.length + 1];
        for (int i = 0; i < index; i++) {
            newArray[i] = ints[i];
        }
        newArray[index] = element;
        for (int i = index; i < ints.length; i++) {
            newArray[i + 1] = ints[i];
        }
        ints = newArray;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public T remove(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("Index out of bounds: " + index);
        }
        T removedElement = ints[index];
        return removedElement;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        // Implementa este método
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        // Implementa este método
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        // Implementa este método
        return false;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        // Implementa este método
        return false;
    }


    @Override
    public boolean retainAll(Collection<?> c) {
        // Implementa este método
        return false;
    }

    @Override
    public void clear() {
        // Implementa este método
    }

    @Override
    public T get(int index) {
        // Implementa este método
        return null;
    }

    @Override
    public T set(int index, T element) {
        // Implementa este método
        return null;
    }


    @Override
    public int indexOf(Object o) {
        // Implementa este método
        return 0;
    }

    @Override
    public int lastIndexOf(Object o) {
        // Implementa este método
        return 0;
    }

    @Override
    public ListIterator<T> listIterator() {
        // Implementa este método
        return null;
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        // Implementa este método
        return null;
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        // Implementa este método
        return null;
    }


}
